<?php

use DI\Definition\FileLoader\YamlDefinitionFileLoader;

// composer autoload
require 'vendor/autoload.php';

// instantiate the container
$container = new DI\ContainerBuilder();

// apply YAML config file to configure the implementation of BarInterface as
// the Bar classz
$container->addDefinitionsFromFile(new YamlDefinitionFileLoader('config/di.yml'));

// instantiate this class with any type hint dependencies resolved. Since Foo
// depends on Bar in the Foo construct, the $container will inject that class
// and the Foo construct will assign it to the local $bar property.
try {
    $foo = $container->build()->get('IdeaInYou\Test\Result');
} catch (\DI\DependencyException $e) {
} catch (\DI\NotFoundException $e) {
}

// without dependency injection this would be:
// $foo = new MikeFunk\Test\Foo;
// but the Bar dependency would not be resolved

// Foo::returnMe() calls Bar::returnMe() and returns it. This should say "this
// is from the bar class"
echo $foo->showResult();
