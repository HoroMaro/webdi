<?php

namespace IdeaInYou\Test;


interface ResultInterface
{

    /**
     * returnMe
     *
     * @return string
     */
    public function showResult();

}
