<?php

namespace IdeaInYou\Test;

use IdeaInYou\Connection\Connect;
use IdeaInYou\Helper\AbstractAuth;

class ChoosenOne extends AbstractAuth implements ResultInterface
{
    public $email;
    public $password;
    /**
     * @var Connect
     */
    private $connect;

    public function __construct(Connect $connect)
   {
       $this->connect = $connect;
       $this->email = $_POST['email'];
       $this->password = $_POST['password'];
       parent::__construct();
   }

    public function showResult()
    {
        $form = $this->render('registration.html.twig', []);
        $conn = $this->connect->connection();
        $email = $this->email;
        $pass = $this->password;

        if ($this->validate($email, $conn)){
             if($this->checkLogin($email, $pass, $conn)){
                 echo $this->render('checkLogin.html.twig', [
                     'message' => 'Here is our Posts!'
                 ]);
             } else {
                 echo $this->render('registration.html.twig', [
                     'message' => 'Please Sign Up!'
                 ]);
             }
            }   else {
            if($this->createUser($conn, $email, $pass)){
                echo $this->render('checkRegistration.html.twig', [
                    'message' => 'User created Successfully!'
                ]);
            } else {
                echo $this->render('checkRegistration.html.twig', [
                    'message' => 'Failed to create User!'
                ]);
            }
        }
        echo $form;
    }

    public function execute()
    {
        return $this->showResult();
    }
}
