<?php

namespace IdeaInYou\Test;


class Result
{

    /**
     * @var ResultInterface
     */
    private $choosen;

    /**
     *
     * @param ResultInterface $choosen
     */
    public function __construct(
        ResultInterface $choosen
    )
    {
        $this->choosen = $choosen;
    }


    public function showResult()
    {
        return $this->choosen->showResult();
    }
}
