<?php

namespace IdeaInYou\Test;


class ChoosenTwo implements ResultInterface
{

    /**
     * just returns a hardcoded string
     *
     * @return string
     */
    public function showResult()
    {
        echo 'This is from the ChoosenTwo class';
    }
}
