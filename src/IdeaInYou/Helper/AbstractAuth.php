<?php

namespace IdeaInYou\Helper;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

abstract class AbstractAuth
{
    public function validate($email,$conn)
    {
        $check_email = mysqli_query($conn, "SELECT * FROM Users WHERE email = '$email'");

        $check_email = mysqli_fetch_assoc($check_email);

        if($check_email){
           return true;
        }else{
            return false;
        }
    }

    public function checkLogin($email, $password,$conn)
    {
        $check_login = mysqli_query($conn, "SELECT * FROM Users WHERE email = '$email' AND password = '$password' ");

        $check_login = mysqli_fetch_assoc($check_login);

        if($check_login){
            return true;
        }else{
            return false;
        }
    }

    public function createUser($conn, $email, $password){
       return mysqli_query($conn,"INSERT INTO Users (id, email, password) VALUES (NULL, '$email', '$password')");
    }

    private $twig;

    public function __construct()
    {
        $loader = new FilesystemLoader(__DIR__ . '/../Templates');
        $this->twig = new Environment($loader);
    }


    abstract public function execute();

    public function render($template, $variables = []) {
        return $this->twig->render($template, $variables);
    }

}
